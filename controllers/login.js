const jwt = require('jsonwebtoken');
const authService = require('../services/auth');

const login = (req, res) => {

  try {

    if (!req.body?.email || !req.body?.password) {

      res.status(400);
      res.send({
        message: 'Invalid email or password'
      });

      return;
    }

    const params = {
      email: req.body.email,
      password: req.body.password
    };

    const serviceRes = authService.login(params);

    if (!serviceRes.ok) {

      res.status(serviceRes.status || 500);
      res.send({
        message: serviceRes.message || 'Unfortunately a technical error occurred'
      });

      return;
    }

    const secret = 'mhjFf98Jj#59#kfng!23knfdke#)plfjNekA'; // need to be moved
    const token = jwt.sign(serviceRes.user, secret);

    res.cookie('Authorization', token, {
      expires: new Date(Date.now() + 15000)
    });

    return res.json({
      user: serviceRes.user,
      token
    });
  }
  catch(e) {

    // we need to log this error

    console.error(e);

    res.status(500);
    res.send({
      message: 'Unfortunately a technical error occurred'
    });
  }
};

module.exports = login;