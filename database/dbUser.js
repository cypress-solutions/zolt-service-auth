const getUserByEmail = (email) => {

  // get user from db, password should be encrypted.  It isn't encrypted just for the sake of the POC.

  const user = email === 'hein.sunkel@gmail.com'
    ? {
        name: 'Hein',
        password: '123456',
        role: 'member'
      }
    : email === 'jm.coetzer3@gmail.com'
      ? {
          name: 'Marco',
          password: '654321',
          role: 'admin'
        }
        : null;

  return user;
};

module.exports = {
  getUserByEmail
};