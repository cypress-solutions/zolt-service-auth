const controllers = require('../controllers');

const ROUTES = {
  login: {
    method: 'POST',
    path: '/identity/login',
    handler: controllers.login,
    isAuthenticated: false,
    roles: []
  }
};

module.exports = ROUTES;