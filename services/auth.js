const dbUser = require('../database/dbUser');

const login = (params) => {

  const user = dbUser.getUserByEmail(params.email);

  if (!user) {

    return {
      message: 'Invalid username or password',
      status: 400,
      ok: false
    };
  }

  if (user.password !== params.password) {

    return {
      message: 'Invalid username or password',
      status: 400,
      ok: false
    };
  }

  return {
    user: {
      email: params.email,
      name: user.name,
      role: user.role
    },
    ok: true
  };
};

module.exports = {
  login
};