var express = require('express');
const ROUTES = require('./routes/routes');
const PORT = 3031;

var app = express();
app.use(express.json());

for (const route of Object.values(ROUTES)) {

  if (!route || !route.method || !route.path || !route.handler) {
    continue;
  }

  switch (route.method.toUpperCase()) {
    case 'GET':
      app.get(route.path, route.handler);
      break;
    case 'POST':
      app.post(route.path, route.handler);
      break;
    case 'PUT':
      app.put(route.path, route.handler);
      break;
    case 'DELETE':
      app.delete(route.path, route.handler);
      break;
    default:
      break;
  }
}

// handle all unhandled errors
app.use(function (err, req, res, next) {

  // we need to log all errors here, still need to be implemented

  console.error(err);

  res.status(500);
  res.send({
    message: 'Unfortunately a technical error occurred'
  });
});

app.listen(PORT, () => {
  console.log(`App listening at http://localhost:${PORT}`);
});